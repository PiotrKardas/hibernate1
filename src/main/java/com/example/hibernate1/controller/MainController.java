package com.example.hibernate1.controller;


import com.example.hibernate1.dao.UserRepository;
import com.example.hibernate1.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/add")
    @ResponseBody
    public String addPerson(@RequestParam String name, @RequestParam String email){
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        userRepository.save(user);
        return "saved";
    }

    @ResponseBody
    @GetMapping("/all")
    public Iterable<User> showAll(){
        return userRepository.findAll();
    }

    @GetMapping("/dodaj")
    public String dodaj(){
        return "dodaj";
    }

    @PostMapping("/result")
    public String result(@ModelAttribute User user, ModelMap modelMap){
        modelMap.addAttribute("user",user);
        userRepository.save(user);
        return "result";
    }

    @GetMapping("/find")
    public String find(){
        return "find";
    }

    @PostMapping("/byname")
    public String byName(@RequestParam String name, ModelMap modelMap){
        modelMap.addAttribute("user",userRepository.findByName(name));
        return "byname";
    }
    @GetMapping("/delete")
    public String deleteId(ModelMap modelMap){
        modelMap.addAttribute("user",userRepository.findAll());
        return "delete";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable Long id, ModelMap modelMap){
        modelMap.addAttribute("user",userRepository.findById(id));
        userRepository.deleteById(id);
        return "deleteinfo";
    }

}
